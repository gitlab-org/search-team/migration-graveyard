# frozen_string_literal: true

require 'optparse'
require 'pathname'
require 'fileutils'

options = {
  path: Pathname.new(File.join(ENV['HOME'], 'gitlab-development-kit', 'gitlab'))
}

parser = OptionParser.new do |opts|
  opts.banner = 'Usage: archive.rb [options]'

  opts.on('-p', '--path PATH', 'Path to gitlab directory. Defaults to ~/gitlab-development-kit/gitlab') do |v|
    options[:path] = v
  end
end

parser.parse!

path = Pathname.new(options[:path])

def_src = path / 'ee' / 'elastic' / 'migrate'
def_dest = Pathname.new('lib/migrate')

definitions = def_src.children.filter do |p|
  File.readlines(p).grep(/include Elastic::MigrationObsolete/).none?
end.sort

spec_src = path / 'ee' / 'spec' / 'elastic' / 'migrate'
spec_dest = Pathname.new('spec/migrate')
versions = definitions.map { |p| p.basename.to_s.split('_').first }
specs = spec_src.children.filter do |p|
  versions.any? { |v| p.basename.to_s.include?(v) }
end.sort

puts 'We are about to do the following. Does this look correct?'
puts
puts '* Copy non-obsolete migrations to lib/migrate'
puts definitions.map(&:basename)
puts
puts '* Copy specs to spec/migrate'
puts specs.map(&:basename)
puts
puts 'Press Ctrl+C to cancel. Enter to continue.'
gets

FileUtils.mkdir_p(def_dest)
FileUtils.cp(definitions, def_dest)

FileUtils.mkdir_p(spec_dest)
FileUtils.cp(specs, spec_dest)

puts 'Done.'
